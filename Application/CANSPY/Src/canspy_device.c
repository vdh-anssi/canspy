/**
  ******************************************************************************
  * @file    canspy_device.c
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   Device module core.
  *          This file provides all the functions to manage devices:
  *           + Registering
  *           + Starting
  *           + Pausing
  *           + Resuming
  *           + Stopping
  *           + Walking
  *           + Finding
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */

#include "canspy_device.h"
#include "canspy_service.h"
#include "canspy_sched.h"
#include "canspy_debug.h"

/** @brief  The default error message for unknown device calls.
  */
char *CANSPY_DEVICE_ERROR_Unknown = "Unknown call %i";

/**
  * @brief  Register a new device.
  * @param  dev_id: the device ID (MUST BE UNIQUE).
  * @param  dev_func: the device function (MUST BE UNIQUE).
  * @param  dev_name: the name of the device (MUST BE UNIQUE).
  * @param  dev_handle: the handle of the device.
  * @param  power: the initial state of the device.
  * @retval Exit Status
  */
int canspy_device_register(CANSPY_DEVICE_ID dev_id, CANSPY_DEVICE_FUNC dev_func, const char *dev_name, void *dev_handle, bool power){

	int len;
	CANSPY_DEVICE_DESC *dev_ptr = CANSPY_DEVICE_POINTER(dev_id);
	CANSPY_OPTION_CALL opt_call;

	dev_ptr->dev_id = dev_id;
	dev_ptr->dev_func = dev_func;
	dev_ptr->dev_opt = malloc(sizeof(CANSPY_OPTION_DESC));
	dev_ptr->dev_opt->var_list = NULL;
	dev_ptr->dev_opt->var_size = 0;
	dev_ptr->dev_name = dev_name;
	dev_ptr->dev_handle = dev_handle;
	dev_ptr->powered = false;
	dev_ptr->paused = false;

	if((len = strlen(dev_name)) > CANSPY_PRINT_MAX_Device)
		CANSPY_PRINT_MAX_Device = len;

	opt_call.dev_ptr = dev_ptr;

	if(dev_func)
		dev_func(DEV_INIT, &opt_call);

	if(power){

		if(canspy_device_start(dev_ptr) != EXIT_SUCCESS)
			CANSPY_DEBUG_ERROR_1(ERROR, "%s: failed to start device", dev_name);
	}

	CANSPY_DEBUG_DEVICE(dev_ptr);

	return EXIT_SUCCESS;
}

/**
  * @brief  Start a registered device.
  * @param  dev_ptr: a pointer to the device structure.
  * @retval Exit Status
  */
int canspy_device_start(CANSPY_DEVICE_DESC *dev_ptr){

	if(dev_ptr->powered)
		return EXIT_FAILURE;

	if(dev_ptr->dev_func != NULL && dev_ptr->dev_func(DEV_START, NULL) != EXIT_SUCCESS){

		if(dev_ptr->dev_func)
			dev_ptr->dev_func(DEV_STOP, NULL);

		return EXIT_FAILURE;
	}

	dev_ptr->powered = true;
	dev_ptr->paused = false;

	return EXIT_SUCCESS;
}

/**
  * @brief  Pause a registered device.
  * @param  dev_ptr: a pointer to the device structure.
  * @retval Exit Status
  */
int canspy_device_pause(CANSPY_DEVICE_DESC *dev_ptr){

	if(!dev_ptr->powered || dev_ptr->paused)
		return EXIT_FAILURE;

	dev_ptr->paused = true;

	return EXIT_SUCCESS;
}

/**
  * @brief  Resume a registered device.
  * @param  dev_ptr: a pointer to the device structure.
  * @retval Exit Status
  */
int canspy_device_resume(CANSPY_DEVICE_DESC *dev_ptr){

	if(!dev_ptr->powered || !dev_ptr->paused)
		return EXIT_FAILURE;

	dev_ptr->paused = false;

	return EXIT_SUCCESS;
}

/**
  * @brief  Stop a registered device.
  * @param  pdevice: a pointer to the device structure.
  * @retval Exit Status
  */
int canspy_device_stop(CANSPY_DEVICE_DESC *dev_ptr){

	int i;

	if(!dev_ptr->powered)
		return EXIT_FAILURE;

	for(i = 0; i < dev_ptr->svc_length; i++){

		if(dev_ptr->svc_list[i].started)
			canspy_service_stop(&(dev_ptr->svc_list[i]), CANSPY_NODEVICE, NULL, NULL);
	}

	if(dev_ptr->dev_func != NULL && dev_ptr->dev_func(DEV_STOP, NULL) != EXIT_SUCCESS)
		return EXIT_FAILURE;

	dev_ptr->powered = false;
	dev_ptr->paused = false;

	return EXIT_SUCCESS;
}

/**
  * @brief  Walk through all devices.
  * @param  cur_dev: the current device or NULL to start.
  * @retval The next device
  */
CANSPY_DEVICE_DESC *canspy_device_walk(CANSPY_DEVICE_DESC *cur_dev){

	if(cur_dev == NULL)
		return CANSPY_DEVICE_POINTER(CANSPY_DEVICE_FIRST);

	if(cur_dev->dev_id >= CANSPY_DEVICES - 1)
		return NULL;

	return CANSPY_DEVICE_POINTER(cur_dev->dev_id + 1);
}

/**
  * @brief  Search for a registered device.
  * @param  dev_id: the id of the device (optional).
  * @param  dev_name: the name of the device (optional).
  * @param  dev_func: the address of the device (optional).
  * @retval A pointer to the device structure
  */
CANSPY_DEVICE_DESC *canspy_device_find(CANSPY_DEVICE_ID dev_id, const char *dev_name, CANSPY_DEVICE_FUNC dev_func){

	CANSPY_DEVICE_DESC *cur_dev = NULL;

	if(dev_name == NULL && dev_func == NULL && dev_id == CANSPY_NODEVICE){

		CANSPY_DEBUG_ERROR_0(ERROR, "At least an id, a name or an address must be specified");
		return NULL;
	}

	if(dev_id != CANSPY_NODEVICE)
		return CANSPY_DEVICE_POINTER(dev_id);

	while((cur_dev = canspy_device_walk(cur_dev)) != NULL){

		if((dev_name != NULL && strncmp(dev_name, cur_dev->dev_name, CANSPY_PRINT_MAX_Device) == 0)
		|| (dev_func != NULL && cur_dev->dev_func == dev_func)){

			return cur_dev;
		}
	}

	return NULL;
}

/**
  * @brief  Walk through all active services for a specified device
  * @param  dev_ptr: the specified device.
  * @param  cur_svc: the current active service or NULL to start.
  * @retval None
  */
CANSPY_SERVICE_DESC *canspy_device_active(CANSPY_DEVICE_DESC *dev_ptr, CANSPY_SERVICE_DESC *cur_svc){

	static bool exclu;

	if(cur_svc == NULL)
		exclu = false;

	while((cur_svc = canspy_service_walk(dev_ptr, cur_svc)) != NULL){

		if(cur_svc->started){

			if(!cur_svc->exclusive || !exclu){

				if(cur_svc->exclusive)
					exclu = true;

				return cur_svc;
			}
		}
	}

	return NULL;
}
