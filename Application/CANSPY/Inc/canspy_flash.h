/**
  ******************************************************************************
  * @file    canspy_flash.h
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   This file contains all the functions prototypes for the
  *          FLASH module core.
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */

#ifndef __CANSPY_FLASH_H_
#define __CANSPY_FLASH_H_

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_flash.h"
#include "stm32f4xx_hal_flash_ex.h"

/* External typedefs ---------------------------------------------------------*/
typedef enum _CANSPY_FLASH_ERROR{

	FLASH_OK = 0,
	FLASH_LEN = -1,
	FLASH_ADDR = -2,
	FLASH_BOUND = -3,
	FLASH_SECTOR = -4
}
CANSPY_FLASH_ERROR;

typedef void (*CANSPY_FLASH_FUNC)(void *);

/* Exported constants --------------------------------------------------------*/
#define CANSPY_FLASH_IDSTR sizeof(uint32_t)
#define CANSPY_FLASH_PRINT sizeof(CANSPY_FLASH_FUNC)
#define CANSPY_FLASH_RESERVED (CANSPY_FLASH_IDSTR + CANSPY_FLASH_PRINT)
#define CANSPY_FLASH_FIRST 1
#define CANSPY_FLASH_CODE 1
#define CANSPY_FLASH_LAST (FLASH_SECTOR_TOTAL - CANSPY_FLASH_CODE)

/* Exported macros -----------------------------------------------------------*/
#define CANSPY_FLASH_CLEAR_FLASG() __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR| FLASH_FLAG_PGSERR)
#define CANSPY_FLASH_HNDL(s) (canspy_flash_sector_addr[s])
#define CANSPY_FLASH_SIZE(s) (canspy_flash_sector_size[s])
#define CANSPY_FLASH_FREE(s) ((*(uint8_t *)CANSPY_FLASH_HNDL(s)) == 0xFF)
#define CANSPY_FLASH_PTR(s) ((void *)(CANSPY_FLASH_HNDL(s) + CANSPY_FLASH_RESERVED))
#define CANSPY_FLASH_LEN(s) (CANSPY_FLASH_SIZE(s) - CANSPY_FLASH_RESERVED)
#define CANSPY_FLASH_IDS(s) ((char *)(CANSPY_FLASH_HNDL(s)))
#define CANSPY_FLASH_PRN(s) ((CANSPY_FLASH_FUNC *)(CANSPY_FLASH_HNDL(s) + CANSPY_FLASH_IDSTR))

/* Exported globals ----------------------------------------------------------*/
extern uint32_t canspy_flash_sector_addr[FLASH_SECTOR_TOTAL];
extern uint32_t canspy_flash_sector_size[FLASH_SECTOR_TOTAL];

/* Exported functions --------------------------------------------------------*/
int canspy_flash_erase(uint32_t sector);
int canspy_flash_write(uint32_t flash_addr, void *data, int size);
void *canspy_flash_search(char flash_id[CANSPY_FLASH_IDSTR]);
void *canspy_flash_assign(size_t size, char flash_id[CANSPY_FLASH_IDSTR], CANSPY_FLASH_FUNC flash_print);
int canspy_flash_store(void **flash_ptr, void *data, int size, bool quick);
int canspy_flash_read(void *src, void *dst, int length, unsigned int *p_read);

#endif /*_CANSPY_FLASH_H*/
