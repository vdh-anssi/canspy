/**
  ******************************************************************************
  * @file    canspy_debug.h
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   This file contains all the functions prototypes for the
  *          Debug module extension.
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */

#ifndef __CANSPY_DEBUG_H_
#define __CANSPY_DEBUG_H_

/* Configs -------------------------------------------------------------------*/
//#define CANSPY_DEBUG_FILTER

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "stm32f4xx_can.h"
#include "canspy_print.h"

/* External typedefs ---------------------------------------------------------*/
typedef enum _CANSPY_SERVICE_FLAG CANSPY_SERVICE_FLAG;

/* Exported typedefs ---------------------------------------------------------*/
typedef enum _CANSPY_DEBUG_LEVEL{

	DBG_INFO,
	DBG_WARN,
	DBG_ERROR,
	DBG_FATAL
}
CANSPY_DEBUG_LEVEL;

/* Globals -------------------------------------------------------------------*/
extern char *CANSPY_MEMORY_ERROR_Alloc;
extern CANSPY_DEBUG_LEVEL CANSPY_DEBUG_SDCARD_Print;
extern CANSPY_DEBUG_LEVEL CANSPY_DEBUG_UART_Print;
extern CANSPY_DEBUG_LEVEL CANSPY_DEBUG_ETH_Print;
extern uint32_t CANSPY_DEBUG_COUNT_Rx[];
extern uint32_t CANSPY_DEBUG_COUNT_Miss[];
extern uint32_t CANSPY_DEBUG_COUNT_Drop[];
extern uint32_t CANSPY_DEBUG_COUNT_Altr[];
extern uint32_t CANSPY_DEBUG_COUNT_Fwrd[];
extern uint32_t CANSPY_DEBUG_COUNT_Tx[];
extern uint32_t CANSPY_DEBUG_COUNT_Fail[];
extern uint32_t CANSPY_DEBUG_COUNT_Inject[];

/* Exported macros -----------------------------------------------------------*/
#define AT __FILE__ ":" TOSTRING(__LINE__) "~"
#define CANSPY_DEBUG_ERROR_0(level, message) canspy_debug_broadcast(DBG_##level, AT, message)
#define CANSPY_DEBUG_ERROR_1(level, format, p1) canspy_debug_broadcast(DBG_##level, AT, format, p1)
#define CANSPY_DEBUG_ERROR_2(level, format, p1, p2) canspy_debug_broadcast(DBG_##level, AT, format, p1, p2)
#define CANSPY_DEBUG_ERROR_3(level, format, p1, p2, p3) canspy_debug_broadcast(DBG_##level, AT, format, p1, p2, p3)
#define CANSPY_DEBUG_ERROR_4(level, format, p1, p2, p3, p4) canspy_debug_broadcast(DBG_##level, AT, format, p1, p2, p3, p4)
#define CANSPY_DEBUG_DEVICE(dev_info) canspy_debug_broadcast(DBG_INFO, NULL, "DEVICE \"%s\": registered%s at 0x%08X (%s)", (dev_info)->dev_name, (dev_info)->dev_func ? "" : " (dummy device)", (dev_info), (dev_info)->powered == true ? "started" : "stopped")
#define CANSPY_DEBUG_SERVICE(svc_info) canspy_debug_broadcast(DBG_INFO, NULL, "SERVICE \"%s\": registered on %s at 0x%08X (%s)", (svc_info)->svc_name, (svc_info)->dev_ptr->dev_name, (svc_info)->svc_func, (svc_info)->started != 0 ? "started" : "stopped")
#define CANSPY_DEBUG_COMMAND(command, proc) canspy_debug_broadcast(DBG_INFO, NULL, "COMMAND \"%s\": registered on SHELL at 0x%08X", command, proc)

/* Exported globals ----------------------------------------------------------*/
extern char *CANSPY_DEBUG_LEVEL_String[];

/* Exported functions --------------------------------------------------------*/
int canspy_debug_message(CANSPY_DEBUG_LEVEL level, void *handle, const char *format, va_list args);
int canspy_debug_print(CANSPY_DEBUG_LEVEL level, void *handle, const char *format, ...);
int canspy_debug_broadcast(CANSPY_DEBUG_LEVEL level, const char *location, const char *format, ...);
int canspy_debug_event(CANSPY_SERVICE_FLAG flag, void *handle);
char *canspy_debug_frame(void *handle, uint32_t temp1, uint32_t temp2, uint8_t can_if, CanRxMsgTypeDef *frame);
int canspy_debug_stats(void *handle, uint8_t can_if, bool reset);

#endif /*_CANSPY_DEBUG_H*/
