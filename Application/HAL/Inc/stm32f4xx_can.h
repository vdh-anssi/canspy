/**
  ******************************************************************************
  * File Name          : stm32f4xx_can.h
  * Description        : This file provides code for the configuration
  *                      of the CAN instances.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2015 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __STM32F4xx_CAN_H
#define __STM32F4xx_CAN_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include "stm32f4xx_hal.h"
	 
/* Exported constants --------------------------------------------------------*/
#define CAN_TX_TIMEOUT 100
#define CAN_DEFAULT_PRESCALER 4
#define CAN1_IF 1
#define CAN2_IF 2
#define CANA_IF 3

/* Exported globals --------------------------------------------------------- */
extern CAN_HandleTypeDef hcan1;
extern CAN_HandleTypeDef hcan2;
extern CanRxMsgTypeDef CAN1_Rx_Mess;
extern CanTxMsgTypeDef CAN1_Tx_Mess;
extern CanRxMsgTypeDef CAN2_Rx_Mess;
extern CanTxMsgTypeDef CAN2_Tx_Mess;
extern uint16_t CAN1_Prescaler;
extern uint16_t CAN2_Prescaler;
	 
/* Exported functions ------------------------------------------------------- */
int CAN_Rx_Handler(uint8_t CanIf);
int CAN_Tx_Handler(uint8_t CanIf);
HAL_StatusTypeDef MX_CAN1_Init(void);
HAL_StatusTypeDef MX_CAN2_Init(void);

#ifdef __cplusplus
}
#endif
#endif /* __STM32F4xx_CAN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
