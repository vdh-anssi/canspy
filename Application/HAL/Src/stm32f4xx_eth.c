/**
  ******************************************************************************
  * File Name          : stm32f4xx_eth.c
  * Description        : This file provides code for the configuration
  *                      of the ETH instances.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2015 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "stm32f4xx_eth.h"

/** @brief  Handle for ETH.
  */
ETH_HandleTypeDef heth;

/** @brief  Buffers for ETH.
  */
uint8_t ETH_Rx_Buf[ETH_RXBUFNB][ETH_MAX_PACKET_SIZE];
uint8_t ETH_Tx_Buf[ETH_TXBUFNB][ETH_MAX_PACKET_SIZE];

/** @brief  Descriptors for ETH DMA.
  */
ETH_DMADescTypeDef ETH_DMA_Rx_Desc[ETH_RXBUFNB];
ETH_DMADescTypeDef ETH_DMA_Tx_Desc[ETH_TXBUFNB];

/** @brief  Default MAC addresses
  */
uint8_t ETH_Mac_Src[6] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8_t ETH_Mac_Dst[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

/** @brief  Configurations for MAC and DMA
  */
ETH_MACInitTypeDef ConfMAC;
ETH_DMAInitTypeDef ConfDMA;

/**
  * @brief  This function initializes ETH DMA.
  */
HAL_StatusTypeDef MX_ETH_DefaultDMA(ETH_HandleTypeDef* heth){
	
	ConfDMA.DropTCPIPChecksumErrorFrame = ETH_DROPTCPIPCHECKSUMERRORFRAME_DISABLE;
	ConfDMA.ReceiveStoreForward = ETH_RECEIVESTOREFORWARD_DISABLE;	
	ConfDMA.TransmitStoreForward = ETH_TRANSMITSTOREFORWARD_DISABLE;
	ConfDMA.ForwardErrorFrames = ETH_FORWARDERRORFRAMES_ENABLE;
	ConfDMA.ForwardUndersizedGoodFrames = ETH_FORWARDUNDERSIZEDGOODFRAMES_ENABLE;
	ConfDMA.SecondFrameOperate = ETH_SECONDFRAMEOPERARTE_ENABLE;
	ConfDMA.AddressAlignedBeats = ETH_ADDRESSALIGNEDBEATS_ENABLE;
	ConfDMA.FixedBurst = ETH_FIXEDBURST_ENABLE;
	ConfDMA.RxDMABurstLength = ETH_RXDMABURSTLENGTH_32BEAT;
	ConfDMA.TxDMABurstLength = ETH_TXDMABURSTLENGTH_32BEAT;
	ConfDMA.DMAArbitration = ETH_DMAARBITRATION_ROUNDROBIN_RXTX_2_1;
	
	/* Deviations from default settings */
	ConfDMA.SecondFrameOperate = ETH_SECONDFRAMEOPERARTE_DISABLE;
  ConfDMA.EnhancedDescriptorFormat = ETH_DMAENHANCEDDESCRIPTOR_ENABLE;
	
	/* Possible errors will handled by the caller */
	return HAL_ETH_ConfigDMA(heth, &ConfDMA);
}

/**
  * @brief  This function initializes ETH MAC.
  */
HAL_StatusTypeDef MX_ETH_DefaultMAC(ETH_HandleTypeDef* heth){
		
	ConfMAC.LoopbackMode = ETH_LOOPBACKMODE_DISABLE;
	ConfMAC.RetryTransmission = ETH_RETRYTRANSMISSION_DISABLE;
	ConfMAC.AutomaticPadCRCStrip = ETH_AUTOMATICPADCRCSTRIP_DISABLE;
	ConfMAC.ReceiveAll = ETH_RECEIVEALL_ENABLE;
	ConfMAC.BroadcastFramesReception = ETH_BROADCASTFRAMESRECEPTION_ENABLE;
	ConfMAC.PromiscuousMode = ETH_PROMISCUOUS_MODE_DISABLE;
	ConfMAC.MulticastFramesFilter = ETH_MULTICASTFRAMESFILTER_PERFECT;
	ConfMAC.UnicastFramesFilter = ETH_UNICASTFRAMESFILTER_PERFECT;
	ConfMAC.ChecksumOffload = ETH_CHECKSUMOFFLAOD_DISABLE;
	
	/* Deviations from default settings */
	ConfMAC.PassControlFrames = ETH_PASSCONTROLFRAMES_BLOCKALL;
	ConfMAC.ZeroQuantaPause = ETH_ZEROQUANTAPAUSE_DISABLE;

	/* Possible errors will handled by the caller */
	return HAL_ETH_ConfigMAC(heth, &ConfMAC);
}

/**
  * @brief  This function initializes ETH.
  */
HAL_StatusTypeDef MX_ETH_Init(void){

  heth.Instance = ETH;
  heth.Init.AutoNegotiation = ETH_AUTONEGOTIATION_DISABLE;
  heth.Init.Speed = ETH_SPEED_100M;
  heth.Init.DuplexMode = ETH_MODE_FULLDUPLEX;
  heth.Init.PhyAddress = 1;
  heth.Init.MACAddr = &ETH_Mac_Src[0];
  heth.Init.RxMode = ETH_RXINTERRUPT_MODE;
  heth.Init.ChecksumMode = ETH_CHECKSUM_BY_SOFTWARE;
  heth.Init.MediaInterface = ETH_MEDIA_INTERFACE_RMII;
	
	if(HAL_ETH_Init(&heth) != HAL_OK)
		return HAL_ERROR;	

	if(HAL_ETH_WritePHYRegister(&heth, PHY_BCR, PHY_RESET) != HAL_OK)
		return HAL_ERROR;
		
	if(MX_ETH_DefaultDMA(&heth) != HAL_OK)
		return HAL_ERROR;
	
	if(MX_ETH_DefaultMAC(&heth) != HAL_OK)
		return HAL_ERROR;
	
	return HAL_OK;
}

/**
  * @brief  This function initializes ETH MSP.
  */
void HAL_ETH_MspInit(ETH_HandleTypeDef* heth){

  GPIO_InitTypeDef GPIO_InitStruct;
  
	if(heth->Instance == ETH){

    /* Peripheral clock enable */
    __ETH_CLK_ENABLE();
  
    /**ETH GPIO Configuration    
    PC1     ------> ETH_MDC
    PA1     ------> ETH_REF_CLK
    PA2     ------> ETH_MDIO
    PA7     ------> ETH_CRS_DV
    PC4     ------> ETH_RXD0
    PC5     ------> ETH_RXD1
    PB11     ------> ETH_TX_EN
    PB12     ------> ETH_TXD0
    PB13     ------> ETH_TXD1 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_4|GPIO_PIN_5;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* Peripheral interrupt init*/
    HAL_NVIC_SetPriority(ETH_IRQn, 6, 0);
    HAL_NVIC_EnableIRQ(ETH_IRQn);
  }
}

/**
  * @brief  This function deinitializes ETH MSP.
  */
void HAL_ETH_MspDeInit(ETH_HandleTypeDef* heth){

  if(heth->Instance == ETH){

    /* Peripheral clock disable */
    __ETH_CLK_DISABLE();
  
    /**ETH GPIO Configuration    
    PC1     ------> ETH_MDC
    PA1     ------> ETH_REF_CLK
    PA2     ------> ETH_MDIO
    PA7     ------> ETH_CRS_DV
    PC4     ------> ETH_RXD0
    PC5     ------> ETH_RXD1
    PB11     ------> ETH_TX_EN
    PB12     ------> ETH_TXD0
    PB13     ------> ETH_TXD1 
    */
    HAL_GPIO_DeInit(GPIOC, GPIO_PIN_1|GPIO_PIN_4|GPIO_PIN_5);
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_7);
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13);

    /* Peripheral interrupt Deinit*/
    HAL_NVIC_DisableIRQ(ETH_IRQn);
  }
} 

/**
  * @brief  This function handles ETH global interrupt.
  */
void ETH_IRQHandler(void){

	/* Setting the blue LED */
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_SET);
	
	/* Signaling that the IRQ is being handled */
	HAL_ETH_IRQHandler(&heth);

	if(HAL_ETH_GetReceivedFrame_IT(&heth) == HAL_OK){
	
		__IO 	ETH_DMADescTypeDef *dmarxdesc;
	
		dmarxdesc = heth.RxFrameInfos.FSRxDesc;
		
		/* Set OWN bit in Rx descriptor: gives the buffers back to DMA */
		for(int i = 0; i < heth.RxFrameInfos.SegCount; i++){
			
			dmarxdesc->Status |= ETH_DMARXDESC_OWN;
			dmarxdesc = (ETH_DMADescTypeDef *)(dmarxdesc->Buffer2NextDescAddr);
		}
		
		/* Handling the IRQ */
		ETH_Rx_Handler(heth.RxFrameInfos.SegCount);
		
		/* Clear segment count */
		heth.RxFrameInfos.SegCount = 0;
		
		/* When Rx Buffer is unavailable the flag is set: clear it and resume reception  */
		if((heth.Instance->DMASR & ETH_DMASR_RBUS) != (uint32_t)RESET){

			/* Clear RBUS ETHERNET DMA flag */
			heth.Instance->DMASR = ETH_DMASR_RBUS;
				
			/* Resume DMA reception */
			heth.Instance->DMARPDR = 0;
		}			
	}
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
