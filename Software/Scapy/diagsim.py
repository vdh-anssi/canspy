## This file is for use with Scapy
## See http://www.secdev.org/projects/scapy for more information
## Authors: Arnaud Lebrun, Jonathan-Christofer Demay
## This program is published under a GPLv2 license
##
#! /usr/bin/python
from scapy.all import *
from random import randint
from threading import Thread
import readline
import code

class OBD_PID(Packet):
    name = 'OBD_PID'
    fields_desc = [
        ByteField('mode', 0),
        ByteField('PID', 0)
    ]

class J1939(Packet):
    name = 'J1939'
    fields_desc = [
        ConditionalField(IntField('pid_support20', 0xffffffff),
			lambda pkt: pkt.underlayer.mode-0x40 in [1,9] and pkt.underlayer.PID == 0x00),
        ConditionalField(ByteField('speed', 0x0),
			lambda pkt: pkt.underlayer.mode-0x40 in [1,2] and pkt.underlayer.PID == 0x0d),
        ConditionalField(StrField('VIN', '0' * 17),
			lambda pkt: pkt.underlayer.mode-0x40 == 9 and pkt.underlayer.PID == 0x02)
    ]

class DiagSim(Thread):
    def __init__(self, eth_iface, can_mac):
        Thread.__init__(self)
        self.eth_iface = eth_iface
        self.can_mac = can_mac
        self.process = True
        self.force = {}
    def run(self):
        while self.process:
            p = next(iter(sniff(iface=self.eth_iface , count=1)), None)
            if p and SocketCAN in p:
                if p[SocketCAN].id == 0x7df or 0x7e0 <= p[SocketCAN].id <= 0x7e7:
                    p.data = ISOTP(p.data)
                    p.data.data = OBD_PID(p.data.data)
                    reply = Ether(dst=self.can_mac) / SocketCAN(id=randint(0x7e8, 0x7ef) if p[SocketCAN].id == 0x7df else p[SocketCAN].id + 8)
                    reply.data = ISOTP() / OBD_PID(mode=p.data.data.mode + 0x40, PID=p.data.data.PID) / J1939()
                    if len(str(reply.data[J1939])) > 0:
                        force = self.force.get(chr(p.data.data.mode) + chr(p.data.data.PID))
                        if force:
                            reply.data[OBD_PID].payload = Raw(force)
                        sendp(reply.fragment(), iface=self.eth_iface, inter=0.2, verbose=False)
                    else:
                        print ("Unsupported OBD Mode/PID: %02x/%02x" % (p.data.data.mode, p.data.data.PID))
    def update(self, mode, pid, data):
        self.force[chr(mode) + chr(pid)] = data
    def stop(self):
        self.process = False

if __name__ == "__main__":
    bind_layers(Ether, SocketCAN, type=0x88b5)
    diagsim = DiagSim('eth1', '00:80:e1:2e:2c:01')
    diagsim.start()
    vars = globals().copy()
    vars.update(locals())
    shell = code.InteractiveConsole(vars)
    shell.interact()
    diagsim.stop()
    diagsim.join()
